package test.mule.dataconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

public class DBManager {

	private static Logger log = Logger.getLogger(DBManager.class);

	public static void createTable(ArrayList<String> filenames, String createSql) throws Exception {

		String[] sqlElements = createSql.split(" ");

		String dbDriver = "org.h2.Driver";
		String dbUrl = "jdbc:h2:file:/tmp/testH2db_" + System.currentTimeMillis();
		String dbUsername = "sa";
		String dbPassword = "";

		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName(dbDriver);
			log.info("Creating a database...");
			connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);

			long startTotal = System.currentTimeMillis();

			log.info("Creating table " + sqlElements[2] + " in given database...");
			statement = connection.createStatement();
			statement.executeUpdate(createSql);
			statement.close();

			log.info("Table created. Inserting records...");
			String sql;
			for (String filename : filenames) {
				long start = System.currentTimeMillis();
				statement = connection.createStatement();
				sql = "INSERT INTO " + sqlElements[2] + " SELECT * FROM CSVREAD('" + filename + "');";
				statement.executeUpdate(sql);
				statement.close();
				log.info("Time to insert records from a file: " + (System.currentTimeMillis() - start) + " ms");
			}

			log.info("Total database creation and data import time: " + (System.currentTimeMillis() - startTotal) + " ms");
			connection.close();

		} catch (SQLException se) {
			se.printStackTrace();
		} finally {
			try {
				if (statement != null)
					statement.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		log.info("Data import done!");
	}
}
