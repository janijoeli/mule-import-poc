package test.mule.dataconnector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class CSVFileSplitter {
	
	private static Logger log = Logger.getLogger(CSVFileSplitter.class);

	public static List<String> split(int linesPerSplit, Object payload) {
		log.info("Begin processing a large CSV stream...");
		long start = System.currentTimeMillis();

		List<String> fileNames = new ArrayList<String>();
		long linesWritten = 0;
		int fileNumber = 1;

		try {
			InputStream inputFileStream = (InputStream) payload;
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputFileStream));

			String line = reader.readLine();

			while (line != null) {
				String fileName = "/tmp/SmallCSVFile_" + String.format("%07d", fileNumber) + ".csv";
				File outFile = new File(fileName);
				fileNames.add(outFile.toString());
				Writer writer = new OutputStreamWriter(new FileOutputStream(outFile));

				while (line != null && linesWritten < linesPerSplit) {
					writer.write(line + System.lineSeparator());
					line = reader.readLine();
					linesWritten++;
				}
				
				writer.close();
				linesWritten = 0;
				fileNumber++;
			}
			
			log.info("Time to process a large CSV stream: " + (System.currentTimeMillis() - start) + " ms");

			reader.close();
			inputFileStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileNames;
	}

}